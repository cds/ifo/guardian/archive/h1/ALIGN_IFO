""" 'update_sus_safesnap.py' will read in the current OPTICALIGN_{P,Y}_OFFSET 
values and use those to overwrite the OPTICALIGN_{P,Y}_OFFSET values listed in 
their respective safe.snap files.
    - Oli Patane 10/15/2024
"""

import fileinput
import shutil
import os
import time
import sys


def opt_dicts(list) -> list:
    """ Creating dictionaries for each optical suspension as:
        {
        'optic1': ['fename', 'susstage'],
        'optic2': ['fename', 'susstage'],
        'optic3': ['fename', 'susstage']
        },
        where 'fename' is the name for the h1sus group, and 'susstage' is the
        stage the sliders are connected to. """
        
    # These suspensions have their offset sliders on M1
    im = {                       # IMs
        'IM1': ['im', 'M1'],
        'IM2': ['im', 'M1'],
        'IM3': ['im', 'M1'],
        'IM4': ['im', 'M1']
    }
    rm = {                       # RMs
        'RM1': ['htts', 'M1'],
        'RM2': ['htts', 'M1']
    }
    mc = {                       # MCs
        'MC1': ['mc1', 'M1'],
        'MC2': ['mc2', 'M1'],
        'MC3': ['mc3', 'M1']
    }
    pr = {                       # PRs
        'PRM': ['prm', 'M1'],
        'PR2': ['pr2', 'M1'],
        'PR3': ['pr3', 'M1']
    }
    bs = {                       # BS
        'BS': ['bs', 'M1']
    }
    sr = {                       # SRs
        'SRM': ['srm', 'M1'],
        'SR2': ['sr2', 'M1'],
        'SR3': ['sr3', 'M1']
    }
    ifo_out = {                  # OMs
        'OMC': ['ifoout', 'M1'],
        'OM1': ['ifoout', 'M1'],
        'OM2': ['ifoout', 'M1'],
        'OM3': ['ifoout', 'M1']
    }
    tms = {                      # TMSs
        'TMSX': ['tmsx', 'M1'],
        'TMSY': ['tmsy', 'M1']
    }
    fc = {                       # SQZ FCs
        'FC1': ['fc1', 'M1'],
        'FC2': ['fc2', 'M1']
    }
    zm_in = {                    # SQZ Ins
        'ZM1': ['sqzin', 'M1'],
        'ZM2': ['sqzin', 'M1'],
        'ZM3': ['sqzin', 'M1']
    }
    zm_out = {                   # SQZ Outs
        'ZM4': ['sqzout', 'M1'],
        'ZM5': ['sqzout', 'M1'],
        'ZM6': ['sqzout', 'M1']
    }
    # These suspensions have their offset sliders on M0
    etm = {                      # ETMs
        'ETMX': ['etmx', 'M0'],
        'ETMY': ['etmy', 'M0']
    }
    itm = {                      # ITMs
        'ITMX': ['itmx', 'M0'],
        'ITMY': ['itmy', 'M0']
    }
    
    # Appending the dictionaries to a list depending on inputs
    opts_list = []
    opt_not_found = []  # Initing list for incorrect/nonexistant optics entered
    for opt in list:
        if opt.lower() == 'all':
            opts_list.append(im)
            opts_list.append(rm)
            opts_list.append(mc)
            opts_list.append(pr)
            opts_list.append(bs)
            opts_list.append(sr)
            opts_list.append(ifo_out)
            opts_list.append(tms)
            opts_list.append(fc)
            opts_list.append(zm_in)
            opts_list.append(zm_out)
            opts_list.append(etm)
            opts_list.append(itm)
            
        elif opt.lower() == 'all_except_sqz':
            opts_list.append(im)
            opts_list.append(rm)
            opts_list.append(mc)
            opts_list.append(pr)
            opts_list.append(bs)
            opts_list.append(sr)
            opts_list.append(ifo_out)
            opts_list.append(tms)
            opts_list.append(etm)
            opts_list.append(itm)
            
        elif opt.lower() == 'im':
            opts_list.append(im)
            
        elif opt.lower() == 'rm':
            opts_list.append(rm)
            
        elif opt.lower() == 'mc':
            opts_list.append(mc)
            
        elif opt.lower() == 'pr':
            opts_list.append(pr)
            
        elif opt.lower() == 'bs':
            opts_list.append(bs)
            
        elif opt.lower() == 'sr':
            opts_list.append(sr)
            
        elif opt.lower() == 'ifo_out':
            opts_list.append(ifo_out)
        elif opt.lower() == 'ifoout':
            opts_list.append(ifo_out)
        elif opt.lower() == 'om':
            opts_list.append(ifo_out)
            
        elif opt.lower() == 'tms':
            opts_list.append(tms)
            
        elif opt.lower() == 'fc':
            opts_list.append(fc)
            
        elif opt.lower() == 'zm_in':
            opts_list.append(zm_in)
        elif opt.lower() == 'sqz_in':
            opts_list.append(zm_in)
        elif opt.lower() == 'sqzin':
            opts_list.append(zm_in)
            
        elif opt.lower() == 'zm_out':
            opts_list.append(zm_out)
        elif opt.lower() == 'sqz_out':
            opts_list.append(zm_out)
        elif opt.lower() == 'sqzout':
            opts_list.append(zm_out)
            
        elif opt.lower() == 'etm':
            opts_list.append(etm)
            
        elif opt.lower() == 'itm':
            opts_list.append(itm)
        # If someone puts in a specific optic (ex. itmx, sr2)
        else:
            all_opts = [        # no bs in here because it's already alone
                im,
                rm,
                mc,
                pr,
                sr,
                ifo_out,
                tms,
                fc,
                zm_in,
                zm_out,
                etm,
                itm
            ]
            temp_dict = {}
            for group in all_opts:
                if opt.upper() in group.keys():
                    temp_dict[opt.upper()] = group[opt.upper()]
                    opts_list.append(temp_dict)
                    break
                elif group == all_opts[-1]:
                    opt_not_found.append(opt)
    return opts_list, opt_not_found


def make_header(list, print_only=False):
    """ Preparing and printing the header info that will be printed at the top
    of the terminal """
    
    # Prepare the string of optic groups in a more user-readable way
    printed_optics = '  '
    for opticgroup in list:
        for optic, fe_sus in opticgroup.items():
            optic = str(optic).upper()
            printed_optics = printed_optics + optic + ' '
    
    # Print basic info to the terminal:
    if print_only:      # Only printing differences
        update_or_not = 'Printing out'
    else:               # Updating values in safe.snap and printing to terminal
        update_or_not = 'Updating and printing out'
    header = f'{update_or_not} saved OPTICALIGN_{{P,Y}}_OFFSET values from ' \
        + f'safe.snap vs current values for these optics:\n{printed_optics}'
    
    return header


def replace_offsets(list, print_only=False) -> list:
    """ Actually goes and grabs current values, prints old vs current values to
    terminal, and if print_only is False, uses them to replace the optic align
    offsets that are in the safe.snap files. """
    
    # Base path for safe.snap in 'lho/h1/target/' that links to the actual file
    #   to be updated (_safe, _observe, etc.)
    BASE_DIR = '/ligo/home/oli.patane/Documents/WIP/safesnap/target/h1sus'#'/opt/rtcds/lho/h1/target/h1sus'
    
    # Need list to append old & new values to to return so we can print or log
    #  depending on usage
    old_new_list = []
    if print_only:
        old = 'Current Saved'
        new = 'Current Value'
    else:
        old = 'Old Saved'
        new = 'New Saved'
    
    for opticgroup in list:
        for optic, fe_sus in opticgroup.items():
            # Getting current values for pitch and yaw
            pchan = f'SUS-{optic}_{fe_sus[1]}_OPTICALIGN_P_OFFSET'
            ychan = f'SUS-{optic}_{fe_sus[1]}_OPTICALIGN_Y_OFFSET'
            cur_vals = [f'{ezca[pchan]:.20e}', f'{ezca[ychan]:.20e}']
            
            # Path that leads to the safe.snap simlink
            SAFE_PATH = f'{BASE_DIR}{fe_sus[0]}/h1sus{fe_sus[0]}epics/burt/safe.snap'
            # Follow symlink to get the actual file that we will be editing
            REAL_PATH = os.readlink(SAFE_PATH)
            
            # Make 2 copies - one we will be editing; other will be old version
            split_path = REAL_PATH.rsplit('/', 1)
            edit_copy = f'/tmp/{split_path[1]}.tmp'
            shutil.copy2(REAL_PATH, edit_copy)
            
            now = time.gmtime()[:6]
            timenow = f'{now[0]}{now[1]}{now[2]}_{now[3]}{now[4]}{now[5]}'
            split_filename = split_path[1].split('.')
            before_copy = f'/tmp/{split_filename[0]}_{timenow}.snap'
            shutil.copy2(REAL_PATH, before_copy)
            
            # Find correct line and replace the offset value
            for line in fileinput.input(edit_copy, inplace=1):
                if line.split(' ')[0] == f'{ezca.prefix}{pchan}':
                    old_row_p = line.split(' ')
                    # Create new line with current value
                    if not print_only:
                        updated_row = ' '.join([
                                            old_row_p[0],
                                            old_row_p[1],
                                            cur_vals[0],
                                            old_row_p[3]
                                            ])
                        line = line.replace(line, updated_row)
                if line.split(' ')[0] == f'{ezca.prefix}{ychan}':
                    old_row_y = line.split(' ')
                    if not print_only:
                        updated_row = ' '.join([
                                            old_row_y[0],
                                            old_row_y[1],
                                            cur_vals[1],
                                            old_row_y[3]
                                            ])
                        line = line.replace(line, updated_row)
                print(line.rstrip())
            # Printing old and new values to terminal
            old_new_list.append(
                f'\n{pchan}\n  {old}: {old_row_p[2]}\n  {new}: {cur_vals[0]}'
            )
            old_new_list.append(
                f'{ychan}\n  {old}: {old_row_y[2]}\n  {new}: {cur_vals[1]}'
            )
        # Once we've gone through the entire file, copy the changes in temp
        #  over to the actual file
        if not print_only:
            # First, verify that the temp file copied over all the way
            with open(REAL_PATH) as OG:
                countOG = sum(1 for _ in OG)
            with open(edit_copy) as TMP:
                countTMP = sum(1 for _ in TMP)
            if countOG == countTMP:  # Copy over updated values
                try:
                    os.system(f'cp {edit_copy} {REAL_PATH}')
                except Exception:
                    print(f'Something went wrong - not updating {REAL_PATH}')
                    sys.exit()
            elif countOG != countTMP:
                print(f'Number of lines don\'t match - not updating {REAL_PATH}')
                sys.exit()
    return old_new_list


if __name__ == '__main__':
    
    import argparse
    import textwrap
    
    import ezca
    ezca = ezca.Ezca()
    
    # Start and end for printed text bolding
    sb = '\033[1m'
    eb = '\033[0;0m'
    
    class CustomHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
        """ For formatting the argparse help text. """
        def _split_lines(self, text, width):
            wrapper = textwrap.TextWrapper(width=width)
            lines = []
            for line in text.splitlines():
                if len(line) > width:
                    lines.extend(wrapper.wrap(line))
                else:
                    lines.append(line)
            return lines
    
    parser = argparse.ArgumentParser(
            prog='update_sus_safesnap',
            formatter_class=CustomHelpFormatter,
            description='Reads the current OPTICALIGN_{P,Y}_OFFSET values and '
                        'uses those to overwrite the OPTICALIGN_{P,Y}_OFFSET '
                        'values in the burt files linked from their '
                        'respective safe.snap files.'
    )
    parser.add_argument(
        '-o',
        '--optics',
        action='store',
        dest='optics',
        type=str,
        nargs='*',
        default=None,
        help=
            'Type the names of optic groups (ex. itm) or '
            'individual optic names (ex. pr2), with spaces in '
            'between. Case does not matter.                 \n'
            'Alternatively, you can type                    \n'
            f'{sb}all{eb} or {sb}all_except_sqz{eb} to update '
            'all optics or all optics except squeeze.       \n'
            f' {sb}OptGroup | Optics       OptGroup | Optics{eb} \n'
            '   itm       ITMX          etm       ETMX      \n'
            '             ITMY                    ETMY      \n'
            '                                               \n'
            '    bs       BS            tms       TMSX      \n'
            '                                     TMSY      \n'
            '    rm       RM1                               \n'
            '             RM2           pr        PRM       \n'
            '                                     PR2       \n'
            '    im       IM1                     PR3       \n'
            '             IM2                               \n'
            '             IM3           mc        MC1       \n'
            '             IM4                     MC2       \n'
            '                                     MC3       \n'
            '    sr       SRM                               \n'
            '             SR2           ifo_out   OMC       \n'
            '             SR3           OR om     OM1       \n'
            '                                     OM2       \n'
            '    fc       FC1                     OM3       \n'
            '             FC2                               \n'
            '                           zm_out    ZM4       \n'
            '    zm_in    ZM1                     ZM5       \n'
            '             ZM2                     ZM6       \n'
            '             ZM3                               \n'
    )
    parser.add_argument(
        '--print-only',
        action='store_true',
        help='Use -p or --print-only to only print the before '
        'and after values without updating the values saved in safe.snap.',
    )
    args = parser.parse_args()
    optics = args.optics
    print_only = args.print_only
    
    # Running through the optics given
    if optics is None:
        print('No optics or groups entered. -h or --help to display OPT-ions.')
    else:
        optics_list, opts_not_found = opt_dicts(optics)
        print('Running $(USERAPPS)/isc/h1/guardian/update_sus_safesnap.py...')
        # Print out any optics that weren't valid
        if len(opts_not_found) > 0:
            not_found = '  '
            print('The following optics were not found in the optic list:')
            for opt in opts_not_found:
                opt = str(opt).upper()
                not_found = not_found + opt + ' '
            print(not_found)
        # Now do the rest
        if len(optics_list) > 0:
            print(make_header(optics_list, print_only=print_only))
            old_new = replace_offsets(optics_list, print_only=print_only)
            for line in old_new:
                print(line)
        print('\n-- Done --')
