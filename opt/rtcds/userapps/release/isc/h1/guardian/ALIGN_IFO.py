# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: $
# $HeadURL: $



#######################################
import time
import cdsutils
from timeout_utils import call_with_timeout
from guardian import GuardState, GuardStateDecorator, NodeManager, Node

import lscparams
import ISC_GEN_STATES
import ISC_library
import update_sus_safesnap as SafeSnapUp 


#######################################
nominal = 'SET_SUS_FOR_FULL_LOCK'

#ca_monitor = True
ca_monitor = False
ca_monitor_notify = False

##################################################
# NODES
##################################################
# Always managed

sus_nodes = ['SUS_ITMY',
         'SUS_ITMX',
         'SUS_PRM',
         'SUS_SRM',
         'SUS_SR2',
         'SUS_PR2',
         'SUS_ETMY', 
         'SUS_ETMX',
         ]

node_list = sus_nodes

nodes = NodeManager(node_list)

# sqz_manager_node = Node('SQZ_MANAGER') # so that we can set requests in prep_sqz_align - Commented out since we currently don't use this node for SQZ alignment - RWS 11Sep2023

##################################################
# functions (could be moved to ISC_library but it seems specific to this guardian)
##################################################
def sus_nodes_done():
    #previously we used nodes.arrived and nodes.done to check if all the suspension nodes are done alinging or misaligning.  Now that we have added SQZ_MANAGER as a node, this doesn't work when you don't care about the state of the squeezer guardian.  I plan to test this next time we get a chance to align the squeezer, then replace the checks in the non-squeezing alignment states with this function before adding SQZ_MANAGER as a node
    flag = True
    for sus_node in sus_nodes:
        if not (nodes[sus_node].arrived and nodes[sus_node].done):
            flag = False
    return flag


##################################################
# STATES: INIT / DOWN
##################################################

class INIT(GuardState):
    request = True

    def main(self):
         log("initializing subordinate nodes...")
         nodes.set_managed()

    def run(self):
         return True


class DOWN(GuardState):
    index = 1
    goto = True

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # Specifically turn off input to DC3/4 (troubleshooting OM saturations)
        ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
        # Turn off length loops
        ezca.get_LIGOFilter('LSC-PRCL1').ramp_gain(0,0) # ramp to 0 over 0 sec
        ezca.get_LIGOFilter('LSC-SRCL1').ramp_gain(0,0)
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(0,1)
        ezca.get_LIGOFilter('LSC-XARM').ramp_gain(0,2)
        ezca.get_LIGOFilter('LSC-YARM').ramp_gain(0,0)
        ezca['LSC-MICH2_RSET'] = 2

        #this will be a problem for trying to use this guardian while ALS is locked for MICH bright.  We can set a check
        if ezca['GRD-ALS_COMM_STATE'] != 'IR_FOUND':
            ezca['IMC-MCL_TRAMP'] = 5
            ezca['IMC-MCL_GAIN'] = 1
        ezca['SQZ-ASC_WFS_SWITCH'] = 0
        # Turn off ASC
        ascList = ['INP1', 'INP2', 'PRC1', 'PRC2', 'SRC1', 'SRC2', 'MICH', 'DC1', 'DC2','DC3','DC4']
        ascList = ['ASC-{0}_'.format(ii) for ii in ascList]
        ascList = [ii+'P' for ii in ascList] + [ii+'Y' for ii in ascList]
        for ascLoop in ascList:
            ezca.get_LIGOFilter(ascLoop).switch_off('INPUT')
            # Trying out a sleep here because the DC3&4 Y dont always get cleared --TJS March 1 2023
            time.sleep(0.05)
            ezca[ascLoop+'_RSET'] = 2
        #also turn off ADS loops
        adsList = ['PIT10', 'YAW10']
        for adsLoop in adsList:
            ezca['ASC-ADS_%s_DOF_GAIN'%adsLoop] = 0 

        #turn off BS dithers used in MICH_BRIGHT_ALIGN
        ezca['ASC-ADS_PIT10_OSC_CLKGAIN']=0
        ezca['ASC-ADS_YAW10_OSC_CLKGAIN']=0
        ezca['ASC-ADS_PIT10_DOF_GAIN']=0
        ezca['ASC-ADS_YAW10_DOF_GAIN']=0

        # Jnote: Stuff that we no longer use??
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').switch_off('INPUT') # why do we need this? PRCL length is off, so no signal input
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').ramp_gain(0, ramp_time=0, wait=False)
        ISC_library.trigrix['SRCL', 'POP_A_DC'] = 0 # Jnote: is this just so it's not annoying?
        # set up BS feedback to M2
        ezca.switch('SUS-BS_M1_LOCK_P','OUTPUT','ON')  # Jnote: do we ever turn this off?
        ezca.switch('SUS-BS_M1_LOCK_Y','OUTPUT','ON')
        ezca['SUS-BS_M2_LOCK_OUTSW_P'] = 0  # Jnote: this turns OFF the switch
        ezca['SUS-BS_M2_LOCK_OUTSW_Y'] = 0
        ezca['ASC-MICH_P_GAIN'] = 0
        ezca['ASC-MICH_Y_GAIN'] = 0
        self.timer['pause'] = 2

        ezca['SUS-BS_M2_LOCK_L_RSET'] = 2

        # Jnote: is this ever used?? Not in Align_IFO orig node, isc_lib, or isc_gen_states
        ezca.switch('SUS-MC2_M3_ISCINF_L', 'FM2', 'OFF')
        
        time.sleep(1)
        # Clear integrators in top masses of suspensions
        optics_list=['IM4','PR2','PRM','SR2','SRM', 'BS', 'RM1', 'RM2', 'OM1', 'OM2']
        for optic in optics_list:
            ezca['SUS-'+optic+'_M1_LOCK_L_RSET']=2
            ezca['SUS-'+optic+'_M1_LOCK_P_RSET']=2
            ezca['SUS-'+optic+'_M1_LOCK_Y_RSET']=2

        ezca['LSC-CONTROL_ENABLE'] = 1

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    #@ISC_library.unstall_nodes(nodes)
    def run(self):
        if ISC_library.is_locked('IMC'):
           return True
        else:
           notify('IMC is not locked')

##################################################
# STATES: PREP ALS / PREP FULL LOCK (SUS only)
##################################################


class SET_SUS_FOR_ALS_FPMI(GuardState):
    index = 5

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('ALS_ONLY', nodes)
        self.timer['wait_for_sus_settle'] = 10

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True


class SET_SUS_FOR_PRMI_W_ALS(GuardState):
    index = 8

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('PRMI_W_ALS', nodes)
        self.timer['wait_for_sus_settle'] = 10

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True


class SET_SUS_FOR_FULL_LOCK(GuardState):
    index = 6

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('FULL_LOCK', nodes)
        self.timer['wait_for_sus_settle'] = 10

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True

class SET_SUS_FOR_MICH(GuardState):
    index = 40

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('MICH', nodes)
        self.timer['wait_for_sus_settle'] = 10

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True
##################################################
# STATES: MICH Length
##################################################

def gen_PREP_FOR_MICH():
    class PREP_FOR_MICH(GuardState):
        #index = 40
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            #ISC_library.set_sus_config('MICH', nodes)
            #self.timer['wait_for_sus_settle'] = 0 # initialize

            ezca['VID-CAM16_EXP'] = 200000

            # Remove triggering
            ISC_library.trigrix['MICH','ASAIR_B_RF90_I'] = 0
            ezca['LSC-MICH_TRIG_THRESH_ON'] = -10  # think about using REFL_A_DC with Thresh_on = 0.005, thresh_off = 0.003
            ISC_library.trigrix['PRCL', 'POPAIR_B_RF90_I'] = 0
            for fm in range(1,11):
                ezca['LSC-MICH_MASK_FM%d'%fm] = 0

            # Ensure fast shutter is open.
            if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                notify('Toast is ready!')
                ezca['ISI-HAM6_SATCLEAR'] = 1  #reset saturation counter
                ezca['ISI-HAM6_WD_RSET'] = 1
                time.sleep(.1)
                ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

            # Length locking settings
            ezca.get_LIGOFilter('LSC-MICH1').only_on('INPUT','OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1','FM2','OUTPUT', 'DECIMATION')
            ISC_library.intrix.put('MICH',[],0) # Zero out MICH row of input matrix
            ISC_library.intrix['MICH', 'ASAIR_A45Q'] = 1
            ezca['LSC-PD_DOF_MTRX_TRAMP'] = 1
            ISC_library.intrix.load()

            #turn off BS top stage,
            ezca['SUS-BS_M1_LOCK_L_GAIN'] = 0
            # engage a roll-off to shave off HF noise, KI Jan-5-2016
            ezca.switch('SUS-BS_M3_ISCINF_L', 'FM6', 'ON')
            ezca.get_LIGOFilter('SUS-BS_M2_LOCK_L').only_on('INPUT','FM4', 'FM5','FM9', 'FM10','OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').only_on('INPUT','FM1', 'FM2','OUTPUT', 'DECIMATION')
            ezca['SUS-BS_M3_ISCINF_L_LIMIT'] = 1000000
            ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'ON')

            # Special request to go to input power for MICH quickly
            ezca['GRD-LASER_PWR_REQUEST'] = 'GOTO_POWER_%sW_FAST'%lscparams.input_power['MICH']

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            # Check to see if BS is moving too much. Thus far a crude max-min over 3 seconds
            # Added to help MICH BRIGHT succeed and not rapidly try to reacquire JSK, TJS, JCD 2019-12-03
            oldata = cdsutils.getdata('SUS-BS_M3_OPLEV_PIT_OUT16',3)
            maxp = oldata.data.max()
            minp = oldata.data.min()
            olpeak2peak = maxp - minp
     
            if ezca['GRD-LASER_PWR_STATE'] == 'GOTO_POWER_%sW_FAST'%lscparams.input_power['MICH']:
                # Make sure end up at final requested state after special power request in main
                ezca['GRD-LASER_PWR_REQUEST'] = 'POWER_%sW'%lscparams.input_power['MICH']
                return False
            elif ezca['GRD-LASER_PWR_STATE'] != 'POWER_%sW'%lscparams.input_power['MICH']:
                # still waiting for power to change
                log( ezca['GRD-LASER_PWR_STATE'])
                notify('LASER_PWR still adjusting')
                return False
            elif olpeak2peak > 0.5: # JSK, TJ, JCD 2019-12-03
                notify('BS pk2pk motion to high, waiting')
                return False
            else:
                return True
    return PREP_FOR_MICH

PREP_FOR_MICH = gen_PREP_FOR_MICH()
PREP_FOR_MICH.index = 41

PREP_FOR_MICH_ALS = gen_PREP_FOR_MICH()
PREP_FOR_MICH_ALS.index = 101

class ACQUIRE_MICH_DARK(GuardState):
    index = 42
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('INPUT')
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_DARK']['ACQUIRE'], ramp_time=2, wait=False)

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ISC_library.is_locked('MICH_DARK'):
            #ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')
            return True
        elif ezca['LSC-MICH2_OUTPUT'] > 1e5:
            notify('MICH is not locking - go to DOWN')

class MICH_DARK_LOCKED(GuardState):
    index = 43
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_DARK']['LOCKED'], ramp_time=2, wait=False)
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.is_locked('MICH_DARK'):
            ezca.get_LIGOFilter('LSC-MICH1').switch_off('FM2') # Turn off integrator
            return 'ACQUIRE_MICH_DARK'
        else:
            return True


class ACQUIRE_MICH_BRIGHT(GuardState):
    index = 51
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('INPUT')
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_BRIGHT']['ACQUIRE'], ramp_time=2, wait=False)

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ISC_library.is_locked('MICH_BRIGHT'):
            return True
        # Move other checks for whether MICH is locked to INIT_ALIGN automatic alignment
        elif ezca['LSC-MICH2_OUTPUT'] > 1e5:
            log('MICH is not locking - go to DOWN')
            return 'DOWN'
        elif abs(ezca['SUS-BS_M2_MASTER_OUT_ULMON']) > 1e5:
            log('MICH is not locking - go to DOWN')
            return 'DOWN'

class ACQUIRE_MICH_BRIGHT_ALS(GuardState):
    index = 102
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('INPUT')
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_BRIGHT']['ACQUIRE'], ramp_time=2, wait=False)

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ISC_library.is_locked('MICH_BRIGHT'):
            return True
        # Move other checks for whether MICH is locked to INIT_ALIGN automatic alignment
        elif ezca['LSC-MICH2_OUTPUT'] > 1e5:
            ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(0, ramp_time=0.5, wait=False)
            notify('BS too far misaligned. Help!')
        elif abs(ezca['SUS-BS_M2_MASTER_OUT_ULMON']) > 1e5:
            ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(0, ramp_time=0.5, wait=False)
            notify('BS too far misaligned. Help!')


class MICH_BRIGHT_LOCKED(GuardState):
    index = 52
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_BRIGHT']['LOCKED'], ramp_time=2, wait=False)
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        # Move other checks for whether MICH is locked to INIT_ALIGN automatic alignment
        if not ISC_library.is_locked('MICH_BRIGHT'):
            log('I do not think MICH is locked on a bright fringe')
            return 'ACQUIRE_MICH_BRIGHT'
        else:
            return True

class MICH_BRIGHT_LOCKED_ALS(GuardState):
    index = 103
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['MICH_BRIGHT']['LOCKED'], ramp_time=2, wait=False)
        ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        # Move other checks for whether MICH is locked to INIT_ALIGN automatic alignment
        if not ISC_library.is_locked('MICH_BRIGHT'):
            log('I do not think MICH is locked on a bright fringe')
            return 'ACQUIRE_MICH_BRIGHT_ALS'
        else:
            return True


##################################################
# STATES: PRC Length
##################################################

class PREP_FOR_PRX(GuardState):
    index = 30
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # if we do MICH or SRC first then come back to PRX< sometimes we end up with 5W, PRX doesn't work well.
        ezca['GRD-LASER_PWR_REQUEST'] = 'POWER_%sW'%lscparams.input_power['PRXY']
        ISC_library.set_sus_config('PRX', nodes)
        self.timer['wait_for_sus_settle'] = 0 # initialize

        # SUS settings
        ezca['SUS-PRM_M2_LOCK_L_RSET'] = 2
        ezca['SUS-PRM_M2_LOCK_OUTSW_L'] = 'ON'
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').only_on('INPUT','DECIMATION', 'OUTPUT')
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').ramp_gain(0,ramp_time=5,wait=False)
        ezca['SUS-PRM_BIO_M2_STATEREQ'] = 2 #set sus back to high range

        # Remove triggering
        ISC_library.trigrix['PRCL', 'POPAIR_B_RF90_I'] = 0
        ezca['LSC-PRCL_TRIG_THRESH_ON'] = -1e4
        for fm in range(1,11):
            ezca['LSC-PRCL_MASK_FM%d'%fm] = 0

        # Length locking settings
        ISC_library.intrix.put('PRCL',[],0) # Zero out PRCL row of input matrix
        ISC_library.intrix['PRCL', 'REFL_A9I'] = 1 *2 #factor of 2 to account for HAM1 vent
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
        time.sleep(0.1)
        ISC_library.intrix.load()

        ezca.get_LIGOFilter('LSC-PRCL1').only_on('DECIMATION', 'OUTPUT')
        for jj in lscparams.filtmods['PRCL']:
            ezca.get_LIGOFilter('LSC-PRCL1').switch_on(jj)
        for jj in lscparams.filtmods['PRM_M2']:
            ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').switch_on(jj)
        ezca['LSC-PRCL1_RSET'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not sus_nodes_done():
            self.timer['wait_for_sus_settle'] = 10
            notify('SUS Guardians working')
        elif not self.timer['wait_for_sus_settle']:
            notify('Waiting for sus to settle')
        elif ezca['GRD-LASER_PWR_STATE'] != 'POWER_%sW'%lscparams.input_power['PRXY']:
            notify('LASER_PWR still adjusting')
        else:
            return True


class ACQUIRE_PRX(GuardState):
    index = 31
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-PRCL1').turn_on('INPUT')
        ezca.get_LIGOFilter('LSC-PRCL1').ramp_gain(lscparams.gain['PRXY'], ramp_time=2, wait=False)

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ISC_library.WFS_DC_centering_servos_OK('REFL') and ISC_library.REFL_PD_OK():
            if ISC_library.PRXY_oscillating():
                return 'DOWN'
            if ISC_library.is_locked('PRX'):
                ezca['SUS-PRM_M2_LOCK_L_GAIN'] = lscparams.sus_crossover['PRM_M2']
                return True


class PRX_LOCKED(GuardState):
    index = 32
    request = False
    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.is_locked('PRX'):
            return 'ACQUIRE_PRX'
        return True


##################################################
# STATES: SRC Length
##################################################

class PREP_FOR_SRY(GuardState):
    index = 60
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('SRY', nodes)
        self.timer['wait_for_sus_settle'] = 30

        #WFS settings that take time
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM1','FM4','FM5', 'OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM1','FM4','FM5', 'OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')

        # SUS settings
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').only_on('DECIMATION','OUTPUT') # do not use SRM top stage
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').only_on('DECIMATION', 'INPUT','OUTPUT')
        ezca['SUS-SRM_M2_LOCK_L_GAIN'] = 1
        ezca['SUS-SRM_M1_LOCK_L_GAIN'] = lscparams.sus_crossover['SRM_M1']
        for jj in lscparams.filtmods['SRM_M1']:
            ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').switch_on(jj)
        ezca['LSC-SRCL1_RSET'] = 2
        ezca.get_LIGOFilter('LSC-SRCL2').ramp_gain(1, ramp_time=2, wait=False)

        # Trigger settings
        ezca['LSC-SRCL_TRIG_THRESH_ON']     = lscparams.thresh['SRXY']['ON']
        ezca['LSC-SRCL_TRIG_THRESH_OFF']    = lscparams.thresh['SRXY']['OFF']
        # This shouldn't actually matter since a few lines down we turn them all off
        ezca['LSC-SRCL_FM_TRIG_THRESH_ON']  = lscparams.thresh['SRXY']['ON']
        ezca['LSC-SRCL_FM_TRIG_THRESH_OFF'] = lscparams.thresh['SRXY']['OFF']
        ISC_library.trigrix['SRCL', 'POPAIR_B_RF18_I'] = 0
        ISC_library.trigrix['SRCL', 'AS_C_NSUM'] = 1
        for fm in range(1,11):
            ezca['LSC-SRCL_MASK_FM%d'%fm] = 0

        # Ensure fast shutter is open.
        if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
            notify('Toast is ready!')
            ezca['ISI-HAM6_SATCLEAR'] = 1  #reset saturation counter
            ezca['ISI-HAM6_WD_RSET'] = 1
            time.sleep(.1)
            ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

        # Jnote: need to config whitening at all? We did when using ASAIR
        # Length locking settings
        ISC_library.intrix.put('SRCL',[],0) # Zero out SRCL row of input matrix
        ISC_library.intrix['SRCL', 'ASAIR_A45Q'] = 1
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
        ISC_library.intrix.load()

        ezca.get_LIGOFilter('LSC-SRCL1').only_on('DECIMATION', 'OUTPUT')
        for jj in lscparams.filtmods['SRCL']:
            ezca.get_LIGOFilter('LSC-SRCL1').switch_on(jj)
        ezca.get_LIGOFilter('LSC-SRCL1').switch_on('FM8') # lowpass needed for SRY, but not full DRMI

        ezca['GRD-LASER_PWR_REQUEST'] = 'GOTO_POWER_%sW_FAST'%lscparams.input_power['SRXY']


    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):


        if ezca['GRD-LASER_PWR_STATE'] == 'GOTO_POWER_%sW_FAST'%lscparams.input_power['SRXY']:
            ezca['GRD-LASER_PWR_REQUEST'] = 'POWER_%sW'%lscparams.input_power['SRXY']
        if not sus_nodes_done():
            self.timer['wait_for_sus_settle'] = 25
            notify('SUS Guardians working')
        elif ezca['GRD-LASER_PWR_STATE'] != 'POWER_%sW'%lscparams.input_power['SRXY']:
            notify('LASER_PWR still adjusting')
        elif sus_nodes_done() and not self.timer['wait_for_sus_settle']:
            notify('Waiting for sus to settle')
        else:
            return True


class ACQUIRE_SRY(GuardState):
    index = 61
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('LSC-SRCL1').turn_on('INPUT')
        ezca.get_LIGOFilter('LSC-SRCL1').ramp_gain(lscparams.gain['SRXY'], ramp_time=2, wait=False)
        #ezca.get_LIGOFilter('LSC-SRCL1').ramp_gain(10000, ramp_time=2, wait=False)
        self.timer['triggers'] = 120
        self.high_triggers = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ISC_library.WFS_DC_centering_servos_OK('REFL') and ISC_library.REFL_PD_OK():
            if ISC_library.is_locked('SRY'):
                ezca.get_LIGOFilter('LSC-SRCL1').switch_on('FM4')
                ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').switch_on('INPUT')
                time.sleep(0.1)
                #ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').switch_on('FM1') #SD, CC, VS, EC commented out on 28Feb2022; SRY locking unstable
                return True
            elif self.timer['triggers']:
                if self.high_triggers:
                    # Check that other conditions are ok first
                    data = call_with_timeout(cdsutils.getdata, 'H1:ASC-AS_A_DC_NSUM_OUTPUT', -30)
                    # To cover the case where no data was returned #thisisathing
                    if not data:
                        return False
                    # If we are getting flashes above 4500 and below 1500 we should be OK
                    if data.data.max() > 4500 and data.data.min() < 1500:
                        # TJS 11Apr2024 commenting this part out until we test out the trig sensor move from POP_A to AS_C
                        # a few more times. I'm keeping this entire elif for now as well in case it helps debug or show oddities
                        #reduction = 0.6
                        #log(f'Conditions look OK, lowering trigger thresholds by {reduction}')
                        #ezca['LSC-SRCL_TRIG_THRESH_ON'] = round(reduction * lscparams.thresh['SRXY']['ON'], 2)
                        #ezca['LSC-SRCL_TRIG_THRESH_OFF'] = round(reduction * lscparams.thresh['SRXY']['OFF'], 2)
                        #ezca['LSC-SRCL_FM_TRIG_THRESH_ON'] = round(reduction * lscparams.thresh['SRXY']['ON'], 2)
                        ##ezca['LSC-SRCL_FM_TRIG_THRESH_OFF'] = round(reduction * lscparams.thresh['SRXY']['OFF'], 2)
                        #self.high_triggers = False
                        #self.timer['triggers'] = 240
                        notify('Possibly well aligned but not catching...')
                    else:
                        # Definitely needs help at this point
                        notify('Try SR2 alignment? Find by hand?')
                else:
                    # This most likely wont catch without help at this point
                    # Not sure we want to force it DOWN though...
                    notify('Try SR2 alignment? Find by hand?')


class SRY_LOCKED(GuardState):
    index = 62
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'SRY'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.is_locked('SRY'):
            return 'ACQUIRE_SRY'
        return True


##################################################
# STATES: X/Y ARM Length
##################################################


def gen_PREP_ARM_IR_state(arm):
    class PREP_ARM_IR(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            if arm == 'X':
                ISC_library.set_sus_config('XARM', nodes)
                NormMtrx_row = 6
            elif arm == 'Y':
                ISC_library.set_sus_config('YARM', nodes)
                NormMtrx_row = 7
            self.timer['wait_for_sus_settle'] = 0 # initialize

            # Reset the offset so the triggering doesnt stop this grd
            avg = cdsutils.avg(-30, 'LSC-TR_{}_QPD_B_SUM_INMON'.format(arm))
            ezca['LSC-TR_{}_QPD_B_SUM_OFFSET'.format(arm)] = -avg

            # Ensure no power normalization is in place
            for col in range(1,19):
                ezca['LSC-POW_NORM_MTRX_%s_15'%NormMtrx_row] = 0

            # Ensure fast shutter is open.
            if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                notify('Toast is ready!')
                ezca['ISI-HAM6_SATCLEAR'] = 1  #reset saturation counter
                ezca['ISI-HAM6_WD_RSET'] = 1
                time.sleep(.1)
                ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

            # SUS settings
            ezca.switch('SUS-MC2_M3_LOCK_L', 'FM10', 'ON', 'FM9', 'OFF')

            # Trigger settings
            ISC_library.trigrix['XARM', 'TRX'] = 1
            ISC_library.trigrix['YARM', 'TRY']= 1
            ezca['LSC-%sARM_TRIG_THRESH_ON'%arm]     = lscparams.thresh['ARM_IR']['ON']
            ezca['LSC-%sARM_TRIG_THRESH_OFF'%arm]    = lscparams.thresh['ARM_IR']['OFF']
            ezca['LSC-%sARM_FM_TRIG_THRESH_ON'%arm]  = lscparams.thresh['ARM_IR_FMs']['ON']
            ezca['LSC-%sARM_FM_TRIG_THRESH_OFF'%arm] = lscparams.thresh['ARM_IR_FMs']['OFF']
            ezca['LSC-%sARM_MASK_FM1'%arm] = 1
            ezca['LSC-%sARM_MASK_FM2'%arm] = 1

            # Length locking settings
            ezca['ALS-C_COMM_PLLSLOW_ENABLE'] = 0 # disable COMM slow feedback to IMC VCO

            ISC_library.intrix.put('%sARM'%arm,[],0) # Zero out arm row of input matrix
            ISC_library.outrix.put('MC2',[],0) # Zero out MC2 row of output matrix
            ISC_library.outrix.put([],'%sARM'%arm,0) # Zero out arm column of output matrix
            ISC_library.outrix['MC2', '%sARM'%arm] = 1
            ISC_library.intrix['%sARM'%arm, 'POP_A45I'] = -320*2 # used to use mtrx elem of 1 for ASAIR45Q, multipiled POP45 element by 32 after whitening gain change. alog 44420  factor of 2 added after HAM1 vent reduced light on POP
            ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
            ISC_library.intrix.load()

            ezca.get_LIGOFilter('LSC-%sARM'%arm).only_on('DECIMATION', 'OUTPUT')
            for jj in lscparams.filtmods['ARM_IR']:
                ezca.get_LIGOFilter('LSC-%sARM'%arm).switch_on(jj)
            ezca['LSC-%sARM_RSET'%arm] = 2

            # As of May 5 2023 wer run with a different whitening gain to compensate for SCRL offset
            # Need to use our old gain settings for this state
            for iq in ['I', 'Q']:
                ezca.get_LIGOFilter(f'LSC-POP_A_RF45_{iq}').only_on('INPUT', 'OFFSET', 'FM1', 'FM4', 'OUTPUT',
                                                                    'DECIMATION')
            ezca['LSC-POP_A_RF45_WHITEN_GAIN'] = 21

            # Get a new offset for POP A RF45 at this new whitening. It will be reverted in SDF revert
            # We need to look at the IN1 channel instead of INMON, see alog62162
            #pop_avg = cdsutils.avg(5, 'LSC-POP_A_RF45_I_IN1')
            old_pop_tramp = ezca['LSC-POP_A_RF45_I_TRAMP']
            ezca['LSC-POP_A_RF45_I_TRAMP'] = 0
            ezca['LSC-POP_A_RF45_Q_TRAMP'] = 0
            time.sleep(0.1)
            #ezca['LSC-POP_A_RF45_I_OFFSET'] = round(-pop_avg, 1)
            ezca['LSC-POP_A_RF45_I_OFFSET'] = -69   #hard coding these values, they need to be updated from time to time when input align stops working. 
            ezca['LSC-POP_A_RF45_Q_OFFSET'] = 57.3
            time.sleep(0.1)
            ezca['LSC-POP_A_RF45_I_TRAMP'] = old_pop_tramp


        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not sus_nodes_done():
                self.timer['wait_for_sus_settle'] = 10
                notify('SUS Guardians working')
            elif sus_nodes_done() and not self.timer['wait_for_sus_settle']:
                notify('Waiting for sus to settle')
            else:
                return True

    return PREP_ARM_IR


def gen_ACQUIRE_ARM_IR_state(arm):
    class ACQUIRE_ARM_IR(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            ezca.get_LIGOFilter('LSC-%sARM'%arm).ramp_gain(lscparams.gain['%sARM_IR'%arm],ramp_time=1,wait=False)
            ezca.get_LIGOFilter('LSC-%sARM'%arm).turn_on('INPUT')

            self.timer['pause'] = 3

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            #if ISC_library.WFS_DC_centering_servos_OK('REFL') and ISC_library.REFL_PD_OK(): # Jnote: why need these? are they the right things here??
            #I don't think it makes sense to check refl WFS here.  We use LSC POP for length sensor, REFL WFS for alignment sensor but we can check that in ALIGNMENT states
            if not ISC_library.is_locked('%sARM'%arm):
                self.timer['pause'] = 0.5
            elif self.timer['pause']:
                ezca.get_LIGOFilter('IMC-MCL').ramp_gain(0,ramp_time=0.5,wait=False)
                return True

    return ACQUIRE_ARM_IR

def gen_ARM_IR_LOCKED_state(arm):
    class ARM_IR_LOCKED(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not ISC_library.is_locked('%sARM'%arm):
                return 'DOWN'
            return True

    return ARM_IR_LOCKED

PREP_XARM_IR = gen_PREP_ARM_IR_state('X')
PREP_XARM_IR.index = 10
ACQUIRE_XARM_IR = gen_ACQUIRE_ARM_IR_state('X')
ACQUIRE_XARM_IR.index = 11
XARM_IR_LOCKED = gen_ARM_IR_LOCKED_state('X')
XARM_IR_LOCKED.index = 12

PREP_YARM_IR = gen_PREP_ARM_IR_state('Y')
PREP_YARM_IR.index = 20
ACQUIRE_YARM_IR = gen_ACQUIRE_ARM_IR_state('Y')
ACQUIRE_YARM_IR.index = 21
YARM_IR_LOCKED = gen_ARM_IR_LOCKED_state('Y')
YARM_IR_LOCKED.index = 22


##################################################
# STATES: WFS DC Centering
##################################################

WFS_CENTERING_XARM = ISC_GEN_STATES.gen_WFS_DC_CENTERING('XARM','AS')
WFS_CENTERING_XARM.index = 13
WFS_CENTERING_YARM = ISC_GEN_STATES.gen_WFS_DC_CENTERING('YARM','AS')
WFS_CENTERING_YARM.index = 23
WFS_CENTERING_PRX  = ISC_GEN_STATES.gen_WFS_DC_CENTERING('PRX','REFL')
WFS_CENTERING_PRX.index = 33
WFS_CENTERING_SRY  = ISC_GEN_STATES.gen_WFS_DC_CENTERING('SRY','AS') # EMC and JCD changed to AS June 28
WFS_CENTERING_SRY.index = 63
WFS_CENTERING_MICH_BRIGHT = ISC_GEN_STATES.gen_WFS_DC_CENTERING('MICH_BRIGHT','AS')
WFS_CENTERING_MICH_BRIGHT.index = 53
WFS_CENTERING_MICH_BRIGHT_ALS = ISC_GEN_STATES.gen_WFS_DC_CENTERING('MICH_BRIGHT','AS')
WFS_CENTERING_MICH_BRIGHT_ALS.index = 104
WFS_CENTERING_MICH_DARK = ISC_GEN_STATES.gen_WFS_DC_CENTERING('MICH_DARK','AS')
WFS_CENTERING_MICH_DARK.index = 44
AS_CENTERING_SINGLE_BOUNCE = ISC_GEN_STATES.gen_WFS_DC_CENTERING('IMC','AS')
AS_CENTERING_SINGLE_BOUNCE.index = 75
##################################################
# STATES: Arm Align (Input Align)
##################################################
def gen_PREP_INPUT_ALIGN(arm):
    class PREP_INPUT_ALIGN(GuardState):
        request = False
        @ISC_library.assert_dof_locked_gen(['IMC', arm + 'ARM'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # set up PR2 (this is different than PR2 ASC set up)
            ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_P').only_on('DECIMATION','OUTPUT','FM2','FM6','INPUT')
            ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_Y').only_on('DECIMATION','OUTPUT','FM2','FM6','INPUT')
            # Turn on IM4 inputs
            ezca.switch('SUS-IM4_M1_LOCK_P', 'INPUT', 'ON')
            ezca.switch('SUS-IM4_M1_LOCK_Y', 'INPUT', 'ON')
            ezca['SUS-PR2_M3_LOCK_OUTSW_P'] = 0
            ezca['SUS-PR2_M3_LOCK_OUTSW_Y'] = 0
            ezca['SUS-PR2_M1_LOCK_P_GAIN'] = 1
            ezca['SUS-PR2_M1_LOCK_Y_GAIN'] = 1
            ezca['SUS-IM4_M1_LOCK_P_GAIN'] = 1
            ezca['SUS-IM4_M1_LOCK_Y_GAIN'] = 1

            # input matrix
            for py in ['PIT','YAW']:
                ezca['SUS-ITMX_M0_LOCK_%s_RSET'%py[0]] = 2 # Clear ITM top stage histories
                ezca['SUS-ITMY_M0_LOCK_%s_RSET'%py[0]] = 2

                ISC_library.asc_intrix[py].put('INP1',[],0) # Zero out INP1 row of input matrix
                ISC_library.asc_intrix[py].put('INP2',[],0) # Zero out INP1 row of input matrix
                ISC_library.asc_intrix[py]['INP1','AS_B_RF45_I'] = 1
                ISC_library.asc_intrix[py]['INP2','AS_A_RF45_I'] = 1

            # output matrix
            for py in ['PIT','YAW']:
                ISC_library.asc_outrix[py].put('PR2',[],0) # Zero out PR2 row of output matrix
                ISC_library.asc_outrix[py].put('IM4',[],0) # Zero out IM4 row of output matrix
                ISC_library.asc_outrix[py]['PR2','INP2'] = 1
                ISC_library.asc_outrix[py]['IM4','INP1'] = 1
            ISC_library.asc_outrix['PIT']['IM4','INP2'] = -125
            ISC_library.asc_outrix['PIT']['IM4','INP2'] = 40  
            # control settings
            ezca.get_LIGOFilter('ASC-INP1_P').only_on('FM5', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-INP1_Y').only_on('FM5', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-INP2_P').only_on('FM5', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-INP2_Y').only_on('FM5', 'OUTPUT', 'DECIMATION')
            ezca['ASC-INP1_P_GAIN'] = -50
            ezca['ASC-INP1_Y_GAIN'] = 10#MC and SED lowered because this was oscillating, Feb 4 2025
            ezca['ASC-INP2_P_GAIN'] = -0.35 # was -0.7, caused oscillation. EDH 2022-11-18
            ezca['ASC-INP2_Y_GAIN'] = -0.7

        @ISC_library.assert_dof_locked_gen(['IMC', arm + 'ARM'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not ISC_library.WFS_DC_centering_servos_OK('AS'):
                notify('AS WFS DC centering')
                return 'WFS_CENTERING_'+arm+'ARM'
            else:
                return True
    return PREP_INPUT_ALIGN

def gen_INPUT_ALIGN(arm):
    class INPUT_ALIGN(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC', arm + 'ARM'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            if not ISC_library.WFS_DC_centering_servos_OK('AS'):
                notify('AS WFS DC centering')
                return 'WFS_CENTERING_'+arm+'ARM'

            ezca.get_LIGOFilter('ASC-INP1_P').switch_on('INPUT')
            ezca.get_LIGOFilter('ASC-INP1_Y').switch_on('INPUT')
            ezca.get_LIGOFilter('ASC-INP2_P').switch_on('INPUT')
            ezca.get_LIGOFilter('ASC-INP2_Y').switch_on('INPUT')
            self.wfs2OffloadList = ['INP1', 'INP2']
            self.wfsTolerancePit = [2,0.06]
            self.wfsToleranceYaw = [0.75, 0.02]
            self.convergenceFlag = False

        @ISC_library.assert_dof_locked_gen(['IMC', arm + 'ARM'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not ISC_library.WFS_DC_centering_servos_OK('AS'):
                notify('AS WFS DC centering')
                return 'WFS_CENTERING_'+arm+'ARM'
            elif self.convergenceFlag:
                return True
            elif ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw):
                    notify('Done waiting')
                    self.convergenceFlag = True
            else:
                notify('waiting for input align to converge')
    return INPUT_ALIGN

PREP_INPUT_ALIGN = gen_PREP_INPUT_ALIGN('X')
PREP_INPUT_ALIGN.index = 14

INPUT_ALIGN = gen_INPUT_ALIGN('X')
INPUT_ALIGN.index = 15

PREP_INPUT_ALIGN_YARM = gen_PREP_INPUT_ALIGN('Y')
PREP_INPUT_ALIGN_YARM.index = 24

INPUT_ALIGN_YARM = gen_INPUT_ALIGN('Y')
INPUT_ALIGN_YARM.index = 25
##################################################
# STATES: PRC Align
##################################################

class PREP_PRC_ALIGN(GuardState):
    index = 34
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','PRX'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):

        # turn on PRM feedback to M3
        ezca['SUS-PRM_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-PRM_M3_LOCK_OUTSW_Y'] = 1
        # turn off PRM feedback to M2
        ezca['SUS-PRM_M2_LOCK_OUTSW_P'] = 0
        ezca['SUS-PRM_M2_LOCK_OUTSW_Y'] = 0
        # turn on PRM feedback to M1
        ezca.switch('SUS-PRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-PRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')

        # Reset the PRC/SRC WFS filters
        ezca['ASC-PRC1_P_RSET'] = 2
        ezca['ASC-PRC1_Y_RSET'] = 2

        # Make sure smooth limiter is off
        ezca['ASC-PRC1_P_SMOOTH_ENABLE'] = 0
        ezca['ASC-PRC1_Y_SMOOTH_ENABLE'] = 0

        for py in ['PIT','YAW']:

            ezca['SUS-ITMX_M0_LOCK_%s_RSET'%py[0]] = 2 # Clear ITM top stage histories
            ezca['SUS-ITMY_M0_LOCK_%s_RSET'%py[0]] = 2

            # input matrix
            ISC_library.asc_intrix[py].put('PRC1',[],0) # Zero out PRM row of input matrix
            #ISC_library.asc_intrix[py]['PRC1', 'REFL_A_RF9_I'] = 1

            # output matrix
            ISC_library.asc_outrix[py].put([],'PRC1',0) # Zero out PRC1 column of output matrix
            ISC_library.asc_outrix[py]['PRM','PRC1'] = 1
        ISC_library.asc_intrix['PIT']['PRC1', 'REFL_B_RF9_I'] = 1 * 2 #factor of 2 to account for HAM1 vent
        ISC_library.asc_intrix['YAW']['PRC1', 'REFL_A_RF9_I'] = 1 * 2

        # control settings
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM2', 'FM3', 'FM5', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM2', 'FM3', 'FM5', 'OUTPUT', 'DECIMATION')

        # DB HY 12/27/18 make the prc1 loop just a simple integrator (from the top mass)
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = 5
        ezca['ASC-PRC1_Y_GAIN'] = 10


    @ISC_library.assert_dof_locked_gen(['IMC','PRX'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_PRX'
        else:
            return True

class PRC_ALIGN(GuardState):
    request = False
    index = 35

    @ISC_library.assert_dof_locked_gen(['IMC','PRX'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_PRX'

        #turn on servos
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')
        #set up for convergence checker
        self.wfs2OffloadList = ['PRC1']
        self.wfsTolerancePit = [50]
        self.wfsToleranceYaw = [50]
        self.convergenceFlag = False

    @ISC_library.assert_dof_locked_gen(['IMC','PRX'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_PRX'
        elif self.convergenceFlag:
            return True
        elif ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw):
            notify('Done waiting')
            self.convergenceFlag = True
        else:
            notify('waiting for prc align to converge')


##################################################
# STATES: MICH Align
##################################################

def gen_PREP_MICH_BRIGHT_ALIGN():
    class PREP_MICH_BRIGHT_ALIGN(GuardState):
        #index = 54
        request = False
        @ISC_library.assert_dof_locked_gen(['IMC','MICH_BRIGHT'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            #set up dither for AS_A_NSUM. Was pit=30000 and yaw=3000.
            CLKGAINs = {'PIT': 1000,
                        'YAW': 300}
            for py in ['PIT', 'YAW']:

                ezca['SUS-ITMX_M0_LOCK_%s_RSET'%py[0]] = 2 # Clear ITM top stage histories
                ezca['SUS-ITMY_M0_LOCK_%s_RSET'%py[0]] = 2

                ISC_library.asc_ads_intrix['%s'%(py)].put('%s10'%(py), [], 0) # Zero out row of input matrix
                ISC_library.asc_ads_intrix['%s'%(py)]['%s10'%(py), 'AS_A_NSUM'] = 1
                ISC_library.asc_ads_lotrix['%s'%(py)].put('BS', [], 0) # Zero out row of LO matrix
                ISC_library.asc_ads_lotrix['%s'%(py)]['BS', 'OSC10'] = 1
                ISC_library.asc_ads_outrix['%s'%(py)].put('BS', [], 0) # Zero out row of output matrix
                ISC_library.asc_ads_outrix['%s'%(py)]['BS', '%s10'%(py)] = 1 # set dither to BS

                ezca['ASC-ADS_%s10_OSC_CLKGAIN'%(py)]=CLKGAINs['%s'%(py)]
                ezca['ASC-ADS_%s10_OSC_SINGAIN'%(py)]=1
                ezca['ASC-ADS_%s10_OSC_COSGAIN'%(py)]=1
                ezca['ASC-ADS_%s10_OSC_TRAMP'%(py)]=3
                ezca['SUS-BS_M2_LOCK_OUTSW_%s'%(py[0])] = 0 # Switch off the M2 Lock out switch
                ezca.get_LIGOFilter('SUS-BS_M1_LOCK_%s'%(py[0])).switch_on('INPUT')

                ezca.get_LIGOFilter('ASC-ADS_%s10_DEMOD_SIG'%(py)).only_on('INPUT', 'OUTPUT', 'DECIMATION','FM2')

            ezca['ASC-ADS_PIT10_OSC_FREQ'] = 8.87
            ezca['ASC-ADS_YAW10_OSC_FREQ'] = 7.20


        @ISC_library.assert_dof_locked_gen(['IMC','MICH_BRIGHT'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            return True
    return PREP_MICH_BRIGHT_ALIGN

PREP_MICH_BRIGHT_ALIGN = gen_PREP_MICH_BRIGHT_ALIGN()
PREP_MICH_BRIGHT_ALIGN.index = 54
PREP_MICH_BRIGHT_ALIGN_ALS = gen_PREP_MICH_BRIGHT_ALIGN()
PREP_MICH_BRIGHT_ALIGN_ALS.index = 105

def gen_MICH_BRIGHT_ALIGN():
    class MICH_BRIGHT_ALIGN(GuardState):
        '''
        Aligns MICH using ASC_AS_A error signals from Alignment Dither System (ADS) oscillators
        '''
        #index = 55
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC','MICH_BRIGHT'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            
            # Yaw seems to naturally go faster on its own.  
            ezca['ASC-ADS_YAW10_DOF_GAIN']=100 # Removed (%py) PJT, Feb 22 2020
            ezca['ASC-ADS_PIT10_DOF_GAIN']=300 # Removed (%py) PJT, Feb 22 2020

            #set up for convergence checker
            self.wfs2OffloadList = ['10']
            self.wfsTolerancePitYaw = [1.5] # Use same thresh for both (even though gains are different)
            
            self.convergenceFlag = False

            self.timer['ramp_on'] = 3

        @ISC_library.assert_dof_locked_gen(['IMC','MICH_BRIGHT'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if self.timer['ramp_on']:
                if not ISC_library.WFS_DC_centering_servos_OK('AS'):
                    notify('AS WFS DC centering')
                    return 'WFS_CENTERING_MICH_BRIGHT'
                elif self.convergenceFlag:
                    # Turn off before offloading. CMC,JCD 18Feb2020 
                    for py in ['PIT', 'YAW']:
                        ezca['ASC-ADS_%s10_DOF_GAIN'%(py)]=0
                    return True
                elif ISC_library.ads_average_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePitYaw, avgDuration=20):
                        # Take a 20 sec avg, see if any dof is not yet done. JCD CMC 18Feb2020
                        notify('Done waiting')
                        self.convergenceFlag = True
                else:
                        notify('waiting for ads mich bright to converge')
    return MICH_BRIGHT_ALIGN
MICH_BRIGHT_ALIGN = gen_MICH_BRIGHT_ALIGN()
MICH_BRIGHT_ALIGN.index = 55
MICH_BRIGHT_ALIGN_ALS = gen_MICH_BRIGHT_ALIGN()
MICH_BRIGHT_ALIGN_ALS.index = 106

class PREP_MICH_DARK_ALIGN(GuardState):
    index = 45
    request = False
    @ISC_library.assert_dof_locked_gen(['IMC','MICH_DARK'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        for py in ['PIT', 'YAW']:

            ezca['SUS-ITMX_M0_LOCK_%s_RSET'%py[0]] = 2 # Clear ITM top stage histories
            ezca['SUS-ITMY_M0_LOCK_%s_RSET'%py[0]] = 2

            ISC_library.asc_intrix[py].put('MICH',[],0) # Zero out MICH row of input matrix
            ISC_library.asc_outrix[py].put([], 'MICH', 0)
            ISC_library.asc_outrix[py].put('BS', 'MICH', 1)

            ezca['ASC-MICH_%s_TRAMP'%py[0]] = 3 # ramping time
            ezca['ASC-MICH_%s_GAIN'%py[0]] = 0 # zero ctrl gain

            if py=='PIT':
                ezca.get_LIGOFilter('ASC-MICH_%s'%(py[0])).only_on\
                    ('OUTPUT', 'DECIMATION','FM1', 'FM3', 'FM7', 'FM10')
                    # removed 'INPUT' otherwise the 0.03 Hz pole in FM3 gets wrong history (GV 2018-09-13)
                ezca.get_LIGOFilter('SUS-BS_M1_LOCK_%s'%py[0]).only_on\
                    ('OUTPUT', 'DECIMATION', 'FM1', 'FM2', 'FM9')

            else:
                ezca.get_LIGOFilter('ASC-MICH_%s'%(py[0])).only_on\
                    ('OUTPUT', 'DECIMATION','FM1', 'FM3', 'FM4', 'FM8', 'FM10')
                    # removed 'INPUT' otherwise the 0.03 Hz pole in FM3 gets wrong history (GV 2018-09-13)
                ezca.get_LIGOFilter('SUS-BS_M1_LOCK_%s'%py[0]).only_on\
                    ('OUTPUT', 'DECIMATION', 'FM1', 'FM9')

            ezca.get_LIGOFilter('SUS-BS_M2_LOCK_%s'%py[0]).only_on\
                    ('INPUT', 'OUTPUT', 'DECIMATION', 'FM5', 'FM7', 'FM9')
            ezca['SUS-BS_M2_LOCK_OUTSW_%s'%py[0]] = 1

            ezca['SUS-BS_M1_LOCK_%s_GAIN'%py[0]] = -1 # note that the gain is -1
            ezca['SUS-BS_M2_LOCK_%s_GAIN'%py[0]] = 1

            # set up input mtrx
            ISC_library.asc_intrix[py].put('MICH', 'AS_B_RF45_I', 1) # error signal

    @ISC_library.assert_dof_locked_gen(['IMC','MICH_DARK'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        return True


class MICH_DARK_ALIGN(GuardState):
    index = 46
    request = False
    '''
    Aligns MICH using ASC_AS_A error signals from Alignment Dither System (ADS) oscillators
    Doesn't really do anything yet.
    '''
    @ISC_library.assert_dof_locked_gen(['IMC', 'MICH_DARK'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #set up for convergence checker
        self.wfs2OffloadList = ['MICH']
        self.wfsTolerancePit = [10]
        self.wfsToleranceYaw = [10]
        self.convergenceFlag = False
        self.cnt = 0

    @ISC_library.assert_dof_locked_gen(['IMC','MICH_DARK'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.cnt==0:
            ezca['ASC-MICH_P_GAIN']=-1400
            ezca['ASC-MICH_Y_GAIN']=-2000
            # switch on inputs only when we are reasy to send output (GV 2018-09-13)
            ezca.get_LIGOFilter('ASC-MICH_P').switch_on('INPUT')
            ezca.get_LIGOFilter('ASC-MICH_Y').switch_on('INPUT')

            self.cnt+=1
            self.timer['wait']=5

        if self.cnt==1 and self.timer['wait']:
            #ezca['ASC-MICH_P_GAIN']=-1000
            #ezca['ASC-MICH_Y_GAIN']=-1000
            #ezca['ASC-MICH_P_GAIN']=-2000
            #ezca['ASC-MICH_Y_GAIN']=-2000

            # HY 09/07/18 somehow the mich dark align stopped working; temp'ly with low-gain setup by leaving the -20dB there
            # GV 2018-09-13 but I see that the -20dB is now removed, which seems to be fine now.
            ezca.get_LIGOFilter('ASC-MICH_P').switch_off('FM1')
            ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('FM1')
            ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_P').ramp_gain(0, ramp_time=10, wait=False)
            ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_Y').ramp_gain(0, ramp_time=10, wait=False)
            self.cnt+=1
            self.timer['wait']=6

        if self.cnt==2 and self.timer['wait']:
            # HY 09/07/18 not turning on the integrator for now
            # GV 2018-09-13 integrators seems to be ok now.
            ezca.get_LIGOFilter('ASC-MICH_P').switch_on('FM2')
            ezca.get_LIGOFilter('ASC-MICH_Y').switch_on('FM2')
            ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P').switch_on('INPUT')
            ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y').switch_on('INPUT')

            self.cnt+=1
            self.timer['wait']=4

        if self.cnt==3 and self.timer['wait']:
            #this convergence check won't work because the signals are too noisy?
            #if ISC_library.ads_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw):
            #    notify('Done waiting')
            return True


##################################################
# STATES: SRC Align
##################################################

class PREP_SRC_ALIGN(GuardState):
    index = 64
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','SRY'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # turn off SRM feedback to M3
        ezca['SUS-SRM_M3_LOCK_OUTSW_P'] = 0
        ezca['SUS-SRM_M3_LOCK_OUTSW_Y'] = 0
        # turn off SRM feedback to M2
        ezca['SUS-SRM_M2_LOCK_OUTSW_P'] = 0
        ezca['SUS-SRM_M2_LOCK_OUTSW_Y'] = 0
        # turn on SRM feedback to M1
        ezca.switch('SUS-SRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-SRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # turn off SR2 feedback to M2
        ezca['SUS-SR2_M3_LOCK_OUTSW_P'] = 0
        ezca['SUS-SR2_M3_LOCK_OUTSW_Y'] = 0
        # turn on SR2 feedback to M1
        ezca.switch('SUS-SR2_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-SR2_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # set up sus
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').only_on('FM5', 'FM9', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').only_on('FM5', 'FM9', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').only_on('FM5', 'FM2', 'FM4', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').only_on('FM5', 'FM2', 'FM4', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')

        # Reset the SRC WFS filters
        ezca['ASC-SRC1_P_RSET'] = 2
        ezca['ASC-SRC1_Y_RSET'] = 2
        ezca['ASC-SRC2_P_RSET'] = 2
        ezca['ASC-SRC2_Y_RSET'] = 2

        for py in ['PIT','YAW']:
            # input matrix
            ISC_library.asc_intrix[py].put('SRC1',[],0) # Zero out SRC1 row of input matrix
            ISC_library.asc_intrix[py].put('SRC2',[],0) # Zero out SRC2 row of input matrix
            ISC_library.asc_intrix[py]['SRC2','AS_C_DC'] = 1
            
        # EMC and JCD updated inmatrx values for SRC1 to use AS only
        ISC_library.asc_intrix['PIT']['SRC1','AS_A_RF45_Q'] = -8
        ISC_library.asc_intrix['PIT']['SRC1','AS_B_RF45_Q'] = -8
        ISC_library.asc_intrix['YAW']['SRC1','AS_A_RF45_Q'] = -6

        for py in ['PIT','YAW']:
            # output matrix
            ISC_library.asc_outrix[py].put([],'SRC1',0) # Zero out SRC1 column of output matrix
            ISC_library.asc_outrix[py].put([],'SRC2',0) # Zero out SRC2 column of output matrix
            ISC_library.asc_outrix[py]['SRM','SRC1'] = 1
            ISC_library.asc_outrix[py]['SR2','SRC2'] = 1
        ISC_library.asc_outrix['PIT']['SRM','SRC2'] = -7.08
        ISC_library.asc_outrix['YAW']['SRM','SRC2'] = 7.12

        # control settings
        ezca['ASC-SRC1_P_LIMIT'] = 100000
        ezca['ASC-SRC1_Y_LIMIT'] = 100000
        ezca['ASC-SRC2_P_LIMIT'] = 100000
        ezca['ASC-SRC2_Y_LIMIT'] = 100000
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM1','FM4','FM5', 'OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM1','FM4','FM5', 'OUTPUT','LIMIT', 'DECIMATION')
        #ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1', 'FM2', 'FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')
        #ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1', 'FM2', 'FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')
        #SED CRC flipped sign of these gains July 22nd 2018

        # JD & HY removed FM1 and FM2 on Aug 10 2018, worked on that day; to be tested in future
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')


        ezca['ASC-SRC1_P_GAIN'] = -2
        ezca['ASC-SRC1_Y_GAIN'] = -2
        ezca['ASC-SRC2_P_GAIN'] = 40
        ezca['ASC-SRC2_Y_GAIN'] = 40

    @ISC_library.assert_dof_locked_gen(['IMC','SRY'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_SRY'
        else:
            return True


class SRC_ALIGN(GuardState):
    index = 65
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','SRY'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_SRY'

        ezca.get_LIGOFilter('ASC-SRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('FM1')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('FM1')

        #set up for convergence checker
        self.wfs2OffloadList = ['SRC1', 'SRC2']
        self.wfsTolerancePit = [2500, 100]
        self.wfsToleranceYaw = [2500, 100]
        self.convergenceFlag = False

        self.timer['wait'] = 10

        self.counter=0

    @ISC_library.assert_dof_locked_gen(['IMC','SRY'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL'):
            notify('REFL WFS DC centering')
            return 'WFS_CENTERING_SRY'
        elif self.counter==2 and self.convergenceFlag and self.timer['wait']:
            return True
        elif self.counter==0 and ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw) and self.timer['wait'] :
            notify('Done waiting')
            #ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('FM1')   # This gain increase causing loops to run away.  Comment out 20Sept2018 JCD
            #ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('FM1')
            self.timer['wait'] = 5
            self.counter+=1
        elif self.counter==1 and ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw) and self.timer['wait'] :
            notify('Done waiting')
            self.timer['wait'] = 2
            self.counter+=1
            self.convergenceFlag = True
        else:
            notify('waiting for src align to converge')


class SR2_ALIGN(GuardState):
    index = 58
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('SR2', nodes) # Single bounce off of ITMY (as in SRY), but with SRM misaligned

        ezca['ASC-SRC2_P_GAIN'] = 40
        ezca['ASC-SRC2_Y_GAIN'] = 40

        # # A few setup things copied from PREP_SRC_ALIGN
        # turn off SR2 feedback to M2
        ezca['SUS-SR2_M3_LOCK_OUTSW_P'] = 0
        ezca['SUS-SR2_M3_LOCK_OUTSW_Y'] = 0
        # turn on SR2 feedback to M1
        ezca.switch('SUS-SR2_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-SR2_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').only_on('FM2', 'FM5', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').only_on('FM2', 'FM5', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1','FM3', 'FM4', 'FM9','FM10', 'OUTPUT', 'LIMIT', 'DECIMATION')

        #set up for convergence checker
        self.wfs2OffloadList = ['SRC2']
        self.wfsTolerancePit = [200]
        self.wfsToleranceYaw = [200]
        self.convergenceFlag = False

        # Disable outputs to SRM, both from SRC1 loop and SRC2 output matrix
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('INPUT')

        # Set input, output matrices
        for py in ['PIT','YAW']:
            ISC_library.asc_intrix[py].put('SRC2',[],0) # Zero out SRC2 row of input matrix
            ISC_library.asc_intrix[py]['SRC2','AS_C_DC'] = 1
            ISC_library.asc_outrix[py]['SRM','SRC2'] = 0 # Do not send any signal to SRM from SRC2
            ISC_library.asc_outrix[py]['SR2','SRC2'] = 1

        self.timer['wait'] = 15
        self.counter = 1

    def run(self):
        if self.timer['wait'] and self.counter == 1:
            ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('INPUT')
            ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('INPUT')

            self.counter += 1
            self.timer['wait'] = 5 # Let ASC go for a moment before increasing gain

        elif self.counter == 2 and self.timer['wait']:
            ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('FM1') # Increase gain by 20dB
            ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('FM1') # Increase gain by 20dB
            self.counter += 1
            self.timer['wait'] = 5 # Let ASC go for a moment before checking for convergence

        elif self.counter == 3 and ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw) and self.timer['wait'] :
            notify('Done waiting')

            self.timer['wait'] = 5
            self.counter += 1

        elif self.counter == 4 and ISC_library.asc_convergence_checker(self.wfs2OffloadList, self.wfsTolerancePit, self.wfsToleranceYaw) and self.timer['wait'] :
            notify('Done waiting')

            self.timer['wait'] = 0 # No need to wait anymore
            self.counter += 1

        elif self.counter == 5 and self.timer['wait']:
            # Ready to go to offload state.
            return True






##################################################
# STATES: SQZ Align
##################################################
class PREP_SINGLE_BOUNCE_Y(GuardState):
    #state that can be used if you need to check that the OMs are well aligned for the IFO beam before you align the squeezer beam to it.
    index = 74
    request = False

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ISC_library.set_sus_config('SINGLE_BOUNCE_Y', nodes)
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1
        #check that the fast shutter is open, and that there is light on the AS diodes
        self.timer['wait_for_sus_settle'] = 0 # initialize

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not sus_nodes_done():
            self.timer['wait_for_sus_settle'] = 10
            notify('SUS Guardians working')
        elif sus_nodes_done() and not self.timer['wait_for_sus_settle']:
            notify('Waiting for sus to settle')
        elif ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] !=0:
            notify('fast shutter is shut!')
        else:
            return True

''' Commented out since we currently don't use this node for SQZ alignment - RWS 11Sep2023

class PREP_SQZ_ALIGN(GuardState):
    index = 84
    request = False

    @ISC_library.unstall_nodes(nodes)
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # lock OPO with SEED
        sqz_manager_note.set_request('LOCKED_SEED_DITHER') # better way to request a non-managed node to go somewhere
        # Misalign PR2, SR2; Align SRM
        notify('Misalign PR2, SR2; Align SRM')
        ISC_library.set_sus_config('SQZ_SINGLE_BOUNCE', nodes)
        self.timer['wait_for_sus_settle']  = 10
        self.counter = 0
        #set input matrix
        #for py in ['P','Y']: # Dan Brown - it was this, wrong keys??? 3/4/19
        for py in ['PIT','YAW']:
            ISC_library.sqz_asc_intrix[py].put('POS',[],0) # Zero out SRC1 row of input matrix
            ISC_library.sqz_asc_intrix[py].put('ANG',[],0) # Zero out SRC2 row of input matrix
            ISC_library.sqz_asc_intrix[py]['POS','AS_A_DC'] = -1
            ISC_library.sqz_asc_intrix[py]['ANG','AS_B_DC'] = -1
        #clear history
        ezca['SQZ-ASC_POS_P_RSET']=2
        ezca['SQZ-ASC_ANG_P_RSET']=2
        ezca['SQZ-ASC_POS_Y_RSET']=2
        ezca['SQZ-ASC_ANG_Y_RSET']=2

    #prevent the squeezer guardian from stalling
    @ISC_library.unstall_nodes(nodes)
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ezca['GRD-SQZ_MANAGER_STATE'] == 'LOCKED_SEED_DITHER':
            notify('waiting for squeezer to prepare seed to inject')
        elif not sus_nodes_done() or not self.timer['wait_for_sus_settle']:
            notify('Waiting for sus to settle')
        else:
            if self.counter ==0:
                # Open SQZ beam diverter
                ezca['SYS-MOTION_C_BDIV_C_OPEN'] = 1
                log('SQZ beam diverter is open.')
                self.counter+=1
            else:
                return True

class SQZ_ALIGN(GuardState):
    index = 85
    request = False

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        self.counter = 0

    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    #this state needs a check to make sure that there is light on the diodes and we aren't sending garbage to the ZMs
    def run(self):
        if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] !=0:
            notify('fast shutter is shut!')
        elif ezca['ASC-AS_A_DC_NSUM_OUT16'] > 4:
            if self.counter ==0:
                ezca.get_LIGOFilter('SQZ-ASC_POS_P').switch_on('INPUT')
                ezca.get_LIGOFilter('SQZ-ASC_POS_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('SQZ-ASC_ANG_P').switch_on('INPUT')
                ezca.get_LIGOFilter('SQZ-ASC_ANG_Y').switch_on('INPUT')
                ezca['SQZ-ASC_WFS_SWITCH'] = 1
                self.counter +=1
            else:
        #ISC_library.WFS_DC_centering_servos_OK(port)
                return True
        else:
            notify('Is there light on the AS diodes?')
'''
##################################################
# STATES: Offload Alignment to sliders
##################################################

OFFLOAD_INPUT_ALIGN   = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('XARM', 10, ['IM4', 'PR2', 'RM1', 'RM2', 'OM1', 'OM2'])
OFFLOAD_INPUT_ALIGN.index = 16
OFFLOAD_INPUT_ALIGN_YARM   = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('YARM', 10, ['IM4', 'PR2', 'RM1', 'RM2', 'OM1', 'OM2'])
OFFLOAD_INPUT_ALIGN_YARM.index = 26
OFFLOAD_PRC_ALIGNMENT = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('PRX', 10, ['PRM', 'RM1', 'RM2'])
OFFLOAD_PRC_ALIGNMENT.index = 36
OFFLOAD_MICH_DARK = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('MICH_DARK', 10, ['OM1', 'OM2', 'BS'])
OFFLOAD_MICH_DARK.index = 47
OFFLOAD_MICH_BRIGHT = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('MICH_BRIGHT', 10, ['OM1', 'OM2', 'BS'])
OFFLOAD_MICH_BRIGHT.index = 56
OFFLOAD_MICH_BRIGHT_ALS = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('MICH_BRIGHT', 10, ['OM1', 'OM2', 'BS'])
OFFLOAD_MICH_BRIGHT_ALS.index = 107
OFFLOAD_SRC_ALIGNMENT = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('SRY', 10, ['SRM','SR2', 'OM1', 'OM2'])
OFFLOAD_SRC_ALIGNMENT.index = 66
OFFLOAD_SR2_ALIGNMENT = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('SR2', 10, ['SR2'])
OFFLOAD_SR2_ALIGNMENT.index = 59
OFFLOAD_AS_CENTERING = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('IMC', 10, ['OM1','OM2'])
OFFLOAD_AS_CENTERING.index = 76

# Commented out since we currently don't use this node for SQZ alignment - RWS 11Sep2023
# OFFLOAD_SQZ_ALIGNMENT = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('SQZ', 10, ['ZM1','ZM2'])
# OFFLOAD_SQZ_ALIGNMENT.index = 86

class INPUT_ALIGN_OFFLOADED(GuardState):
    index = 17
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        return True

class INPUT_ALIGN_YARM_OFFLOADED(GuardState):
    index = 27
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        return True

class PRC_ALIGN_OFFLOADED(GuardState):
    index = 37
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        return True

class MICH_DARK_OFFLOADED(GuardState):
    index = 48
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca['ASC-MICH_P_GAIN']=0
        ezca['ASC-MICH_Y_GAIN']=0
        ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')

        ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_P').ramp_gain(300, ramp_time=10, wait=False)
        ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_Y').ramp_gain(650, ramp_time=10, wait=False)
        return True

def gen_MICH_BRIGHT_OFFLOADED():
    class MICH_BRIGHT_OFFLOADED(GuardState):
        #index = 57
        request = True

        @ISC_library.assert_dof_locked_gen(['IMC'])
        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            return True
    return MICH_BRIGHT_OFFLOADED
MICH_BRIGHT_OFFLOADED = gen_MICH_BRIGHT_OFFLOADED()
MICH_BRIGHT_OFFLOADED.index = 57
MICH_BRIGHT_OFFLOADED_ALS = gen_MICH_BRIGHT_OFFLOADED()
MICH_BRIGHT_OFFLOADED_ALS.index = 108

class SRC_ALIGN_OFFLOADED(GuardState):
    index = 67
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca['LSC-SRCL1_GAIN']=0 # Jnote: why do we unlock the SRC?

        # Turn off DC1 and DC2 to stop RMs saturating 
        ascList = ['DC1', 'DC2'] 
        ascList = ['ASC-{0}_'.format(ii) for ii in ascList] 
        ascList = [ii+'P' for ii in ascList] + [ii+'Y' for ii in ascList]
        for ascLoop in ascList: 
            ezca.get_LIGOFilter(ascLoop).switch_off('INPUT') 
            ezca[ascLoop+'_RSET'] = 2 

        # Clear history of RMs for DC centering 
        for dof in ['P', 'Y']: 
            for optic in ['RM1','RM2']: 
                ezca['SUS-{}_M1_LOCK_{}_RSET'.format(optic, dof)] = 2 



        return True

''' Commented out since we currently don't use this node for SQZ alignment - RWS 11Sep2023

class SQZ_ALIGN_OFFLOADED(GuardState):
    index = 87
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca['SQZ-ASC_WFS_SWITCH'] = 0
        self.timer['pause'] = 3
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1

        # Turn off DC1 and DC2 to stop RMs saturating
        ascList = ['DC1', 'DC2']
        ascList = ['ASC-{0}_'.format(ii) for ii in ascList]
        ascList = [ii+'P' for ii in ascList] + [ii+'Y' for ii in ascList]
        for ascLoop in ascList:
            ezca.get_LIGOFilter(ascLoop).switch_off('INPUT')
            ezca[ascLoop+'_RSET'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['pause']:

            return True
'''

class AS_CENTERING_OFFLOADED(GuardState):
    index = 77
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        return True


##################################################
# STATES: Lower laser power after SRC align
##################################################

class LOWER_POWER(GuardState):
    #index = -20
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # turn off WFS
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').switch_off('INPUT')

        ezca['GRD-LASER_PWR_REQUEST'] = 'POWER_2W'

        ezca['VID-CAM16_EXP'] = 1200  # set AS PORT CAM

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if ezca['GRD-LASER_PWR_STATE'] == 'POWER_2W':
            return True

####################
# If wanting to update snap file slider values for optics

class UPDATE_SAFE_SNAP(GuardState):
    """Updates the OPTICALIGN slider values in the safe.snap files"""
    request = True
    index = 115
    def main(self):
        
        # Determine the optic groups that we want to update offset values for
        optics = ['all_except_sqz']
        optics_list, opts_not_found = SafeSnapUp.opt_dicts(optics)
        log('Running $(USERAPPS)/isc/h1/guardian/update_sus_safesnap.py...')
        log(SafeSnapUp.make_header(optics_list))
        old_new_values = SafeSnapUp.replace_offsets(optics_list)
        for line in old_new_values:
            log(line)
        log('\n--Done--')

##################################################
# EDGES
##################################################

edges = [
        # SUS setup
        ('DOWN',                  'SET_SUS_FOR_ALS_FPMI'),
        ('SET_SUS_FOR_ALS_FPMI',  'SET_SUS_FOR_FULL_LOCK'),
        ('SET_SUS_FOR_ALS_FPMI',  'SET_SUS_FOR_PRMI_W_ALS'),
        ('SET_SUS_FOR_FULL_LOCK', 'SET_SUS_FOR_ALS_FPMI'),
        ('SET_SUS_FOR_PRMI_W_ALS','SET_SUS_FOR_ALS_FPMI'),
        ('SET_SUS_FOR_FULL_LOCK', 'SET_SUS_FOR_PRMI_W_ALS'),
        ('SET_SUS_FOR_PRMI_W_ALS','SET_SUS_FOR_FULL_LOCK'),
        # MICH
        ('DOWN',                  'SET_SUS_FOR_MICH'),
        ('SET_SUS_FOR_MICH',      'PREP_FOR_MICH'),
        ('PREP_FOR_MICH',         'ACQUIRE_MICH_DARK'),
        ('ACQUIRE_MICH_DARK',     'MICH_DARK_LOCKED'),
        ('MICH_DARK_LOCKED',      'WFS_CENTERING_MICH_DARK'),
        ('WFS_CENTERING_MICH_DARK', 'PREP_MICH_DARK_ALIGN'),
        ('PREP_MICH_DARK_ALIGN',  'MICH_DARK_ALIGN'),
        ('MICH_DARK_ALIGN',       'OFFLOAD_MICH_DARK'),
        ('OFFLOAD_MICH_DARK',     'MICH_DARK_OFFLOADED'),
        #MICH BRIGHT
        ('PREP_FOR_MICH',         'ACQUIRE_MICH_BRIGHT'),
        ('ACQUIRE_MICH_BRIGHT',   'MICH_BRIGHT_LOCKED'),
        ('MICH_BRIGHT_LOCKED',    'WFS_CENTERING_MICH_BRIGHT'),
        ('WFS_CENTERING_MICH_BRIGHT', 'PREP_MICH_BRIGHT_ALIGN'),
        ('PREP_MICH_BRIGHT_ALIGN', 'MICH_BRIGHT_ALIGN'),
        ('MICH_BRIGHT_ALIGN',       'OFFLOAD_MICH_BRIGHT'),
        ('OFFLOAD_MICH_BRIGHT',     'MICH_BRIGHT_OFFLOADED'),
        ('MICH_DARK_LOCKED',      'ACQUIRE_MICH_BRIGHT'),
        ('MICH_BRIGHT_LOCKED',    'ACQUIRE_MICH_DARK'),
        # PRX
        ('DOWN',                  'PREP_FOR_PRX'),
        ('PREP_FOR_PRX',          'ACQUIRE_PRX'),
        ('ACQUIRE_PRX',           'PRX_LOCKED'),
        ('PRX_LOCKED',            'WFS_CENTERING_PRX'),
        ('WFS_CENTERING_PRX',     'PREP_PRC_ALIGN'),
        ('PREP_PRC_ALIGN',        'PRC_ALIGN'),
        ('PRC_ALIGN',             'OFFLOAD_PRC_ALIGNMENT'),
        ('OFFLOAD_PRC_ALIGNMENT', 'PRC_ALIGN_OFFLOADED'),
        # SRY
        ('DOWN',                  'PREP_FOR_SRY'),
        ('PREP_FOR_SRY',          'ACQUIRE_SRY'),
        ('ACQUIRE_SRY',           'SRY_LOCKED'),
        ('SRY_LOCKED',            'WFS_CENTERING_SRY'),
        ('WFS_CENTERING_SRY',     'PREP_SRC_ALIGN'),
        ('PREP_SRC_ALIGN',        'SRC_ALIGN'),
        ('SRC_ALIGN',             'OFFLOAD_SRC_ALIGNMENT'),
        ('OFFLOAD_SRC_ALIGNMENT', 'LOWER_POWER'),
        ('LOWER_POWER',           'SRC_ALIGN_OFFLOADED'),
        # SR2
        ('DOWN',                  'SR2_ALIGN'),
        ('SR2_ALIGN',             'OFFLOAD_SR2_ALIGNMENT'),
        # XARM
        ('DOWN',                  'PREP_XARM_IR'),
        ('PREP_XARM_IR',          'ACQUIRE_XARM_IR'),
        ('ACQUIRE_XARM_IR',       'XARM_IR_LOCKED'),
        ('XARM_IR_LOCKED',        'WFS_CENTERING_XARM'),
        ('WFS_CENTERING_XARM',    'PREP_INPUT_ALIGN'),
        ('PREP_INPUT_ALIGN',      'INPUT_ALIGN'),
        ('INPUT_ALIGN',           'OFFLOAD_INPUT_ALIGN'),
        ('OFFLOAD_INPUT_ALIGN',   'INPUT_ALIGN_OFFLOADED'),
        # YARM
        ('DOWN',                  'PREP_YARM_IR'),
        ('PREP_YARM_IR',          'ACQUIRE_YARM_IR'),
        ('ACQUIRE_YARM_IR',       'YARM_IR_LOCKED'),
        ('YARM_IR_LOCKED',        'WFS_CENTERING_YARM'),
        ('WFS_CENTERING_YARM',    'PREP_INPUT_ALIGN_YARM'),
        ('PREP_INPUT_ALIGN_YARM',      'INPUT_ALIGN_YARM'),
        ('INPUT_ALIGN_YARM',           'OFFLOAD_INPUT_ALIGN_YARM'),
        ('OFFLOAD_INPUT_ALIGN_YARM',   'INPUT_ALIGN_YARM_OFFLOADED'),
        # SQZ
#        ('DOWN',                  'PREP_SQZ_ALIGN'),
#        ('PREP_SQZ_ALIGN',        'SQZ_ALIGN'),
#        ('SQZ_ALIGN',             'OFFLOAD_SQZ_ALIGNMENT'),
#        ('OFFLOAD_SQZ_ALIGNMENT',     'SQZ_ALIGN_OFFLOADED'),
        #Single bounce off of ITMY to check AS centering before doing the squeezer alignment
        ('DOWN',                  'PREP_SINGLE_BOUNCE_Y'),
        ('PREP_SINGLE_BOUNCE_Y',  'AS_CENTERING_SINGLE_BOUNCE'),
        ('AS_CENTERING_SINGLE_BOUNCE','OFFLOAD_AS_CENTERING'),
        ('OFFLOAD_AS_CENTERING',    'AS_CENTERING_OFFLOADED'),
        #a MICH branch to be used with ALS locked (to replace CHECK_MICH_FRINGES)
        ('SET_SUS_FOR_ALS_FPMI',   'PREP_FOR_MICH_ALS'),
        ('PREP_FOR_MICH_ALS',       'ACQUIRE_MICH_BRIGHT_ALS'),
        ('ACQUIRE_MICH_BRIGHT_ALS', 'MICH_BRIGHT_LOCKED_ALS'),
        ('MICH_BRIGHT_LOCKED_ALS',    'WFS_CENTERING_MICH_BRIGHT_ALS'),
        ('WFS_CENTERING_MICH_BRIGHT_ALS', 'PREP_MICH_BRIGHT_ALIGN_ALS'),
        ('PREP_MICH_BRIGHT_ALIGN_ALS', 'MICH_BRIGHT_ALIGN_ALS'),
        ('MICH_BRIGHT_ALIGN_ALS',       'OFFLOAD_MICH_BRIGHT_ALS'),
        ('OFFLOAD_MICH_BRIGHT_ALS',     'MICH_BRIGHT_OFFLOADED_ALS'),
        # UPDATING SAFE.SNAP
        ('DOWN',                         'UPDATE_SAFE_SNAP'),
        ('UPDATE_SAFE_SNAP',                         'DOWN')
        ]
